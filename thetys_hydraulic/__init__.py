"""
 Provide some hydraulic tools
 
"""
__license__ = "GNU GPL"


##======================================================================================================================##
##                PACKAGES                                                                                              ##
##======================================================================================================================##

import argparse
import doctest

from thetys_hydraulic.relations import *
from thetys_hydraulic.pipe import *
from thetys_hydraulic.hyd_head import *

##======================================================================================================================##
##                MAIN                                                                                                  ##
##======================================================================================================================##

if __name__ == '__main__':
    
    #+++++++++++++++++++++++++++++++#
    #    DOCTESTS                   #
    #+++++++++++++++++++++++++++++++#
    parser = argparse.ArgumentParser(description='')
    parser.add_argument('--doctests', default=False, action='store_true')
    opts = parser.parse_args()
    
    #+++++++++++++++++++++++++++++++#
    #    DOCTEST                    #
    #+++++++++++++++++++++++++++++++#
    if opts.doctests:
        test_result = doctest.testmod()
        print(test_result)



    pipe = CircularPipe(d = 0.03)


    p_m = pipe.perimeter
    s_m = pipe.area

    kpvc = 120
    slope= 0.02

    bazin_pvc = 0.06

    cpvc = bazin_formula(bazin_pvc, pm=p_m, sm=s_m)
    print(q_manning_strickler(kpvc, pm = p_m, sm=s_m, slope=slope)*60e3, "l/min")
    print(q_chezy(cpvc, pm = p_m, sm=s_m, slope=slope)*60e3, "l/min")



    for h in (1e-2, 2e-2, 5e-2, 0.1, 0.2, pipe.d):
        print(h, 'm')
        max_speed = head2speed(h)
        assert abs(speed2head(max_speed) - h) < 1e-8
        print(round(max_speed,3), "m/s")
        q_max = pipe.speed2flow(max_speed)
        print(round(q_max, 4), "m3/s")
        print(round(q_max*1000, 1), "l/s")
        print("\n"*3)


        
    for h in (1e-2, 2e-2, 5e-2, 0.1, 0.2, pipe.d):
        h_in_pipe = HydraulicHead(z=0.02+h)
        v_in_bottom_pipe = h_in_pipe.head_transfer("v").v
        q_bottom = pipe.speed2flow(v_in_bottom_pipe)
        print("h", h, "m")
        print("q", q_bottom*1000, "l/s")
        print("\n")
