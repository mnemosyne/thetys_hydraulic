"""
 Hydraulic Head tools
"""

##======================================================================================================================##
##                PACKAGES                                                                                              ##
##======================================================================================================================##

import argparse
import doctest
# ~ import math
import pandas

from apollon_physics import GRAV_ACCEL
from chaos_substances import RHO_LIQUID_WATER

##======================================================================================================================##
##                CONSTANT                                                                                              ##
##======================================================================================================================##

_head = "head"



##======================================================================================================================##
##                CLASS                                                                                                 ##
##======================================================================================================================##


    
        
class HydraulicHead:
    """ Class doc
    >>> 'afarie' """
    
    UNITS = {'v': "m2/s", 'p': "Pa", 'z' : "m"}
    
    def __init__(self, z=0, p=0, v=0, unit="m"):
        """ Class initialiser """
        self.v = v
        self.p = p
        self.z = z
        
        if unit == "m":    
            self.pressure_head = p / (GRAV_ACCEL*RHO_LIQUID_WATER)
            self.velocity_head = v**2 / (2*GRAV_ACCEL)
            self.gravity_head = z
        elif unit == "Pa":
            self.pressure_head = p
            self.velocity_head = RHO_LIQUID_WATER * v**2 / 2
            self.gravity_head = GRAV_ACCEL * RHO_LIQUID_WATER * z
        else:
            raise ValueError("Not implemented")
        self.unit = unit
        self.total_head = self.pressure_head + self.velocity_head + self.gravity_head
    
    def _base_keys(self):
        """
        Function doc
        >>> 'afarie'
        """
        res = sorted(self.UNITS)
        return res
        
    
    def _head_keys(self):
        """
        Function doc
        >>> 'afarie'
        """
        _total = "total"
        
        res0 = [k for k in self.__dict__ if _head in k]
        # ~ print(res0)
        istotal = [_total in i for i in res0]
        idx = istotal.index(True)
        thead = res0.pop(idx)
        
        res = sorted(res0) + [thead]
        return res
        
    def sorted_keys(self):
        """
        Function doc
        >>> 'afarie'
        """
        res = self._base_keys() + self._head_keys()
        return res
    
    def units(self):
        """
        Function doc
        >>> 'afarie'
        """
        base = self.UNITS.copy()
        others = {k : self.unit for k in self.__dict__.keys() if _head in k}
        base.update(others)
        res = pandas.Series(base)
        res = res.reindex(self.sorted_keys())
        
        return res
        
    def values(self):
        """
        Function doc
        >>> 'afarie'
        """
        res = pandas.Series(self.__dict__)
        res = res.reindex(self.sorted_keys())
        return res
                
    
    def __add__(self, other):
        """
        Function doc
        >>> 'afarie'
        """
        new_p = self.p + other.p
        new_z = self.z + other.z
        new_v = self.v + other.v
        
        res = HydraulicHead(z=new_z, p=new_p, v=new_v, unit=self.unit)
        return res
    
    def __sub__(self, other):
        """
        Function doc
        >>> 'afarie'
        """
        new_p = self.p - other.p
        new_z = self.z - other.z
        new_v = self.v - other.v
        
        res = HydraulicHead(z=new_z, p=new_p, v=new_v, unit=self.unit)
        return res
    
    def __str__(self):
        """
        Function doc
        >>> 'afarie'
        """
        df = pandas.concat([self.values(), self.units()], axis=1)
        res = df.to_string(float_format="{0:.2f}".format)
        
        return res
    
    def head_transfer(self, typ):
        """
        Function doc
        >>> 'afarie'
        """
        if self.unit== "Pa":
            if typ == "v":
                res = HydraulicHead(v=v, unit=self.unit)
            elif typ == "p":
                res = HydraulicHead(p=p, unit=self.unit)
            elif typ == "z":
                res = HydraulicHead(z=z, unit=self.unit)
            else:
                raise ValueError(f"Expected some of {self._base_keys()}")
        
        elif self.unit== "m":
        
            if typ == "v":
                v = (self.total_head * 2*GRAV_ACCEL) **0.5
                res = HydraulicHead(v=v, unit=self.unit)
            elif typ == "p":
                p = self.total_head * RHO_LIQUID_WATER * GRAV_ACCEL
                res = HydraulicHead(p=p, unit=self.unit)
            elif typ == "z":
                z = self.total_head
                res = HydraulicHead(z=z, unit=self.unit)
            else:
                raise ValueError(f"Expected some of {self._base_keys()}")

        else:
            raise ValueError("Pb code")

        # ~ print(res)
        assert abs(res.total_head - self.total_head) < 1e-8
        
        return res
    
    


##======================================================================================================================##
##                MAIN                                                                                                  ##
##======================================================================================================================##


if __name__ == '__main__':
    
    #+++++++++++++++++++++++++++++++#
    #    DOCTESTS                   #
    #+++++++++++++++++++++++++++++++#
    parser = argparse.ArgumentParser(description='')
    parser.add_argument('--doctests', default=False, action='store_true')
    opts = parser.parse_args()
    
    #+++++++++++++++++++++++++++++++#
    #    DOCTEST                    #
    #+++++++++++++++++++++++++++++++#
    if opts.doctests:
        test_result = doctest.testmod()
        print(test_result)
        
        
      
