"""
 Pipe Classes 
"""

##======================================================================================================================##
##                PACKAGES                                                                                              ##
##======================================================================================================================##

import argparse
import doctest
import math

##======================================================================================================================##
##                PIPE CLASSES                                                                                          ##
##======================================================================================================================##


  

class Pipe:
    """ Class doc
    >>> 'afarie' """
    
    def __init__(self, perimeter, area, **kwargs):
        """ Class initialiser """
        self.perimeter = perimeter
        self.area = area
    
    def wet_area(self, h=None):
        """
        Function doc
        >>> 'afarie'
        """
        if h is None:
            res = self.area
        else:
            raise ValueError("Not implemented")

        return res
        
    def wet_perimeter(self, h=None):
        """
        Function doc
        >>> 'afarie'
        """
        if h is None:
            res = self.perimeter
        else:
            raise ValueError("Not implemented")

        return res
        
    
    def speed2flow(self, speed, h=None):
        """
        Function doc
        >>> 'afarie'
        """
        res = self.wet_area(h=h) * speed
        return res


class CircularPipe(Pipe):
    """ Class doc
    >>> 'afarie' """
    
    def __init__(self, d, **kwargs):
        """ Class initialiser """
        a = math.pi*d**2/4
        p = math.pi*d
        
        self.d = d
        
        Pipe.__init__(self, perimeter = p, area=a)



##======================================================================================================================##
##                MAIN                                                                                                  ##
##======================================================================================================================##

if __name__ == '__main__':
    
    #+++++++++++++++++++++++++++++++#
    #    OPTIONS                    #
    #+++++++++++++++++++++++++++++++#
    
    parser = argparse.ArgumentParser(description='')
    parser.add_argument('--doctests', default=False, action='store_true')
    opts = parser.parse_args()
    
    #+++++++++++++++++++++++++++++++#
    #    DOCTESTS                   #
    #+++++++++++++++++++++++++++++++#
    
    if opts.doctests:
        test_result = doctest.testmod()
        print(test_result)
