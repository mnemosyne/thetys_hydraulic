"""
 Hydraulic relations
"""

##======================================================================================================================##
##                PACKAGES                                                                                              ##
##======================================================================================================================##

import argparse
import doctest

from apollon_physics import GRAV_ACCEL
from chaos_substances import RHO_LIQUID_WATER

##======================================================================================================================##
##                CONSTANT                                                                                              ##
##======================================================================================================================##



##======================================================================================================================##
##                FUNCTIONS                                                                                             ##
##======================================================================================================================##

def head2speed(head):
    """
    Function doc
    >>> 'afarie'
    """
    vsquared = head * 2*GRAV_ACCEL
    res = vsquared ** 0.5
    return res
    
def speed2head(speed):
    """
    Function doc
    >>> 'afarie'
    """
    res = speed **2 / (2*GRAV_ACCEL)
    return res

#tableau des manning https://sites.uclouvain.be/didacticiel-hydraulique/Lecons/Lecon_II_1/Tableau_n.htm
#PVC : http://sotici.com/WordPresSotici/wp-content/uploads/2017/01/DocAssa.3.pdf


def bazin_formula(bazin_coef, pm, sm):
    """
    Function doc
    >>> 'afarie'
    """
    rh = hydraulic_radius(pm=pm, sm=sm)
    den = 1 + bazin_coef / rh **0.5
    res = 87 / den
    return res
    

def v_chezy(c, pm, sm, slope):
    """
    Function doc
    >>> 'afarie'
    """
    rh = hydraulic_radius(pm=pm, sm=sm)
    res = c * (slope*rh)**0.5
    return res


def manning2strickler(n):
    """
    Function doc
    >>> 'afarie'
    """
    res = 1/n
    return res

def strickler2manning(k):
    """
    Function doc
    >>> 'afarie'
    """
    res = 1/k
    return res

def hydraulic_radius(pm, sm):
    """
    Function doc
    >>> 'afarie'
    """
    res = sm/pm
    return res

def v_manning_strickler(k, pm, sm, slope):
    """
    Function doc
    >>> 'afarie'
    """
    rh = hydraulic_radius(pm=pm, sm=sm)
    res = k * rh**(2/3) * slope**0.5
    return res
    
def q_manning_strickler(k, pm, sm, slope):
    """
    Function doc
    >>> 'afarie'
    """
    v = v_manning_strickler(k=k, pm=pm, sm=sm, slope=slope)
    res = v*sm
    return res
    
def q_chezy(c, pm, sm, slope):
    """
    Function doc
    >>> 'afarie'
    """
    v = v_chezy(c=c, pm=pm, sm=sm, slope=slope)
    res = v*sm
    return res

def hydraulic_power(q, h):
    """
    Function doc
    >>> 'afarie'
    """
    res = RHO_LIQUID_WATER * GRAV_ACCEL * q * h
    return res


##======================================================================================================================##
##                MAIN                                                                                                  ##
##======================================================================================================================##


if __name__ == '__main__':
    
    #+++++++++++++++++++++++++++++++#
    #    DOCTESTS                   #
    #+++++++++++++++++++++++++++++++#
    parser = argparse.ArgumentParser(description='')
    parser.add_argument('--doctests', default=False, action='store_true')
    opts = parser.parse_args()
    
    #+++++++++++++++++++++++++++++++#
    #    DOCTEST                    #
    #+++++++++++++++++++++++++++++++#
    if opts.doctests:
        test_result = doctest.testmod()
        print(test_result)
